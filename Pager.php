<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

class Pager
{
    private $pageSize = 10;
    private $numberOfPages;

    public function paginate(iterable $list, int $page): Generator
    {
        $this->numberOfPages = ceil(count($list) / $this->pageSize);

        $startIdx = $this->pageSize * ($page - 1);
        $endIdx = $startIdx + $this->pageSize - 1;
        for ($i = $startIdx; $i <= $endIdx; $i++) {
            if (!empty($list[$i])) {
                yield $list[$i];
            }
        }
    }

    public function setPageSize(int $size): void
    {
        $this->pageSize = $size;
    }

    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }
}

$eol = "<br/>";
$p = new Pager();
$p->setPageSize(7);
echo sprintf("pagee size is %d %s", $p->getPageSize(), $eol);

$list = range(1, 100);

$pageNo = 1;
foreach($p->paginate($list, $pageNo) as $item) {
    echo sprintf("item %s, page:%s/%s%s", $item, $pageNo, $p->getNumberOfPages(), $eol);
}
echo $eol;

$pageNo = 2;
foreach($p->paginate($list, $pageNo) as $item) {
    echo sprintf("item %s, page:%s/%s%s", $item, $pageNo, $p->getNumberOfPages(), $eol);
}
echo $eol;

$pageNo = 3;
foreach($p->paginate($list, $pageNo) as $item) {
    echo sprintf("item %s, page:%s/%s%s", $item, $pageNo, $p->getNumberOfPages(), $eol);
}
echo $eol;

echo mt_rand();
